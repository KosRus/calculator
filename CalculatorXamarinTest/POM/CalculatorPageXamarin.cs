﻿using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;
using Xamarin.UITest;


namespace CalculatorXamarinTest.POM
{
   public class CalculatorPageXamarin
    {
        public static IApp App => AppInitializer.App;
        public static void Repl()
        {
            App.Repl();
        }
        public Query textField => x => x.Id("input");
        public Query btnDEL = x => x.Marked("DEL");
        public Query btnZero = x => x.Marked("0");
        public Query btnOne = x => x.Marked("1");
        public Query btnTwo = x => x.Marked("2");
        public Query btnThree = x => x.Marked("3");
        public Query btnFour = x => x.Marked("4");
        public Query btnFive = x => x.Marked("5");
        public Query btnSix = x => x.Marked("6");
        public Query btnSeven = x => x.Marked("7");
        public Query btnEight = x => x.Marked("8");
        public Query btnNine = x => x.Marked("9");
        public Query btnPoint = x => x.Marked(".");
        public Query btnPlus = x => x.Marked("+");
        public Query btnMinus = x => x.Marked("-");
        public Query btnMultiply = x => x.Marked("×");
        public Query btnDivide = x => x.Marked("/");
        public Query btnEqual = x => x.Marked("=");
        public CalculatorPageXamarin TapOnOne()
        {
            App.Tap(btnOne); 
            return this;
        }
        public CalculatorPageXamarin TapOnPlus()
        {
            App.Tap(btnPlus);
            return this;
        }
        public CalculatorPageXamarin TapOnTwo()
        {
            App.Tap(btnTwo);
            return this;
        }
        public CalculatorPageXamarin TapOnThree()
        {
            App.Tap(btnThree);
            return this;
        }
        public CalculatorPageXamarin TapOnFour()
        {
            App.Tap(btnFour);
            return this;
        }
        public CalculatorPageXamarin TapOnFive()
        {
            App.Tap(btnFive);
            return this;
        }
        public CalculatorPageXamarin TapOnSix()
        {
            App.Tap(btnSix);
            return this;
        }
        public CalculatorPageXamarin TapOnSeven()
        {
            App.Tap(btnSeven);
            return this;
        }
        public CalculatorPageXamarin TapOnEight()
        {
            App.Tap(btnEight);
            return this;
        }
        public CalculatorPageXamarin TapOnNine()
        {
            App.Tap(btnNine);
            return this;
        }
        public CalculatorPageXamarin TapOnZero()
        {
            App.Tap(btnZero);
            return this;
        }
        public CalculatorPageXamarin TapOnEqual()
        {
            App.Tap(btnEqual);
            return this;
        }
        public CalculatorPageXamarin TapOnMinus()
        {
            App.Tap(btnMinus);
            return this;
        }
        public CalculatorPageXamarin TapOnMultiply()
        {
            App.Tap(btnMultiply);
            return this;
        }
        public CalculatorPageXamarin TapOnDivide()
        {
            App.Tap(btnDivide);
            return this;
        }
        public CalculatorPageXamarin TapOnPoint()
        {
            App.Tap(btnPoint);
            return this;
        }
       
        public string GetTextFromField()
        {
            return App.Query(textField)[0].Text.Trim();
        }
        public CalculatorPageXamarin TapOnDel()
        {
            App.Tap(btnDEL);
            return this;
        }
    }
}
