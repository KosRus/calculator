﻿using System;
using TechTalk.SpecFlow;
using CalculatorXamarinTest.POM;
using NUnit.Framework;

namespace CalculatorXamarinTest.Steps
{
    [Binding]
    public class CalculatorXamarinSteps
    {   
        public CalculatorPageXamarin calculator;

        [BeforeScenario]
        public void Connect()
        {
            AppInitializer.StartApp();
        }
        [Then(@"result is four")]
        public void ThenResultIsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromField());
        }


        [When(@"tap to button four")]
        public void WhenTapToButtonFour()
        {
            calculator.TapOnFour();
        }

        [When(@"tap to button  multiply")]
        public void WhenTapToButtonMultiply()
        {
            calculator.TapOnMultiply();
        }

        [When(@"tap to button seven")]
        public void WhenTapToButtonSeven()
        {
            calculator.TapOnSeven();
        }

        [When(@"tap to button equal")]
        public void WhenTapToButtonEqual()
        {
            calculator.TapOnEqual();
        }

        [Then(@"results twenty-seven")]
        public void ThenResultsTwenty_Seven()
        {
            Assert.AreEqual("27", calculator.GetTextFromField());
        }

        [When(@"tap to button six")]
        public void WhenTapToButtonSix()
        {
            calculator.TapOnSix();
        }

        [When(@"tap to button division")]
        public void WhenTapToButtonDivision()
        {
            calculator.TapOnDivide();
        }

        [When(@"tap to button two")]
        public void WhenTapToButtonTwo()
        {
            calculator.TapOnTwo();
        }

        [Then(@"results three")]
        public void ThenResultsThree()
        {
            Assert.AreEqual("3", calculator.GetTextFromField());
        }

        [When(@"tap to button eight")]
        public void WhenTapToButtonEight()
        {
            calculator.TapOnEight();
        }

        [Then(@"results twenty-eight")]
        public void ThenResultsTwenty_Eight()
        {
            Assert.AreEqual("28", calculator.GetTextFromField());
        }

        [When(@"tap to button nine")]
        public void WhenTapToButtonNine()
        {
            calculator.TapOnNine();
        }

        [When(@"tap to button plus")]
        public void WhenTapToButtonPlus()
        {
            calculator.TapOnPlus();
        }

        [Then(@"results seventeen")]
        public void ThenResultsSeventeen()
        {
            Assert.AreEqual("17", calculator.GetTextFromField());
        }

        [When(@"tap to button minus")]
        public void WhenTapToButtonMinus()
        {
            calculator.TapOnMinus();
        }

        [When(@"tap to button five")]
        public void WhenTapToButtonFive()
        {
            calculator.TapOnFive();
        }

        [Then(@"results four")]
        public void ThenResultsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromField());
        }

        [When(@"tap to button dot")]
        public void WhenTapToButtonDot()
        {
            calculator.TapOnPoint();
        }

        [Then(@"results three and a half")]
        public void ThenResultsThreeAndAHalf()
        {
            Assert.AreEqual("3,5", calculator.GetTextFromField());
        }

        [Given(@"calculator with the number five is open")]
        public void GivenCalculatorWithTheNumberFiveIsOpen()
        {
            calculator.TapOnFive();
        }

        [When(@"tap to button DEL")]
        public void WhenTapToButtonDEL()
        {
            calculator.TapOnDel ();
        }

        [When(@"tap to button one")]
        public void WhenTapToButtonOne()
        {
            calculator.TapOnOne();
        }

        [When(@"tap to button zero")]
        public void WhenTapToButtonZero()
        {
            calculator.TapOnZero();
        }

        [Then(@"results one")]
        public void ThenResultsOne()
        {
            Assert.AreEqual("1", calculator.GetTextFromField());
        }
        [AfterScenario]
        public void Clear()
        {
            calculator.TapOnDel();
        }
        
    }
}
