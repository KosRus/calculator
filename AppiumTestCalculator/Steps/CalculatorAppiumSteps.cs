﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;
using AppiumTestCalculator.POM;

namespace AppiumTestCalculator.Steps
{
    [Binding]
    public class CalculatorAppiumSteps
    {
        public CalculatorPageAppium calculator;
        public AndroidDriver<AndroidElement> driver;
        [BeforeScenario]
        public void Connect()
        {
            AppiumOptions caps = new AppiumOptions();


            caps.AddAdditionalCapability("app", @"A:\\ApkCalc\\com.companyname.calculaor.apk");
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("deviceName", "Google Pixel XL");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("build", "CSharp Android_Sim");
            caps.AddAdditionalCapability("name", "first_test");

            driver = new AndroidDriver<AndroidElement>(
                   new Uri("http://localhost:4723/wd/hub"), caps);
            calculator = new CalculatorPageAppium(driver);

        }


        [Then(@"result is four")]
        public void ThenResultIsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromResult());
        }
       

        [When(@"tap to button four")]
        public void WhenTapToButtonFour()
        {
            calculator.FourButton();
        }

        [When(@"tap to button  multiply")]
        public void WhenTapToButtonMultiply()
        {
            calculator.MultiplyButton();
        }

        [When(@"tap to button seven")]
        public void WhenTapToButtonSeven()
        {
            calculator.SevenButton();
        }

        [When(@"tap to button equal")]
        public void WhenTapToButtonEqual()
        {
            calculator.EqualButton();
        }

        [Then(@"results twenty-seven")]
        public void ThenResultsTwenty_Seven()
        {
            Assert.AreEqual("27", calculator.GetTextFromResult());
        }

        [When(@"tap to button six")]
        public void WhenTapToButtonSix()
        {
            calculator.SixButton();
        }

        [When(@"tap to button division")]
        public void WhenTapToButtonDivision()
        {
            calculator.DivisionButton();
        }

        [When(@"tap to button two")]
        public void WhenTapToButtonTwo()
        {
            calculator.TwoButton();
        }

        [Then(@"results three")]
        public void ThenResultsThree()
        {
            Assert.AreEqual("3", calculator.GetTextFromResult());
        }

        [When(@"tap to button eight")]
        public void WhenTapToButtonEight()
        {
            calculator.EightButton();
        }

        [Then(@"results twenty-eight")]
        public void ThenResultsTwenty_Eight()
        {
            Assert.AreEqual("28", calculator.GetTextFromResult());
        }

        [When(@"tap to button nine")]
        public void WhenTapToButtonNine()
        {
            calculator.NineButton();
        }

        [When(@"tap to button plus")]
        public void WhenTapToButtonPlus()
        {
            calculator.AddButton();
        }

        [Then(@"results seventeen")]
        public void ThenResultsSeventeen()
        {
            Assert.AreEqual("17", calculator.GetTextFromResult());
        }

        [When(@"tap to button minus")]
        public void WhenTapToButtonMinus()
        {
            calculator.MinusButton();
        }

        [When(@"tap to button five")]
        public void WhenTapToButtonFive()
        {
            calculator.FiveButton();
        }

        [Then(@"results four")]
        public void ThenResultsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromResult());
        }

        [When(@"tap to button dot")]
        public void WhenTapToButtonDot()
        {
            calculator.DotButton();
        }

        [Then(@"results three and a half")]
        public void ThenResultsThreeAndAHalf()
        {
            Assert.AreEqual("3,5", calculator.GetTextFromResult());
        }

        [Given(@"calculator with the number five is open")]
        public void GivenCalculatorWithTheNumberFiveIsOpen()
        {
            calculator.FiveButton();
        }

        [When(@"tap to button DEL")]
        public void WhenTapToButtonDEL()
        {
            calculator.DelButton();
        }

        [When(@"tap to button one")]
        public void WhenTapToButtonOne()
        {
            calculator.OneButton();
        }

        [When(@"tap to button zero")]
        public void WhenTapToButtonZero()
        {
            calculator.ZeroButton();
        }

        [Then(@"results one")]
        public void ThenResultsOne()
        {
            Assert.AreEqual("1", calculator.GetTextFromResult());
        }
        [AfterScenario]
        public void Clear()
        {
            calculator.DelButton();
        }

    }
}
