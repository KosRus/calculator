﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TestCalculator.POM
{
    public class CalculatorPage
    {
        public AndroidDriver<AndroidElement> driver;

        public CalculatorPage(AndroidDriver<AndroidElement> driver)
        {
            this.driver = driver;
        }

        public By oneButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[11]");
        public By twoButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]");
        public By threeButton = By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[13]");
        public By fourButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[7]");
        public By fiveButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[8]");
        public By sixButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[9]");
        public By sevenButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3]");
        public By eightButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4]");
        public By nineButton = By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[5]");
        public By zeroButton = By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[16]");
        public By minusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[10]");
        public By plusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]");
        public By dotButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[15]");
        public By delButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1]");
        public By divisionButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2]");
        public By multiButton = By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6]");
        public By equalButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]");
        public By result = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView");
        
        public CalculatorPage OneButton()
        {
            driver.FindElement(oneButton).Click();
            return this;
        }
        public CalculatorPage TwoButton()
        {
            driver.FindElement(twoButton).Click();
            return this;
        }
        public CalculatorPage AddButton()
        {
            driver.FindElement(plusButton).Click();
            return this;
        }
        public CalculatorPage EqualButton()
        {
            driver.FindElement(equalButton).Click();
            return this;
        }
        public AndroidElement Result()
        {
            return driver.FindElement(result);
        }
        public string GetTextFromResult()
        {
            return Result().Text;
        }
        public CalculatorPage ThreeButton()
        {
            driver.FindElement(threeButton).Click();
            return this;
        }
        public CalculatorPage FourButton()
        {
            driver.FindElement(fourButton).Click();
            return this;
        }
        public CalculatorPage FiveButton()
        {
            driver.FindElement(fiveButton).Click();
            return this;
        }
        public CalculatorPage SixButton()
        {
            driver.FindElement(sixButton).Click();
            return this;
        }
        public CalculatorPage SevenButton()
        {
            driver.FindElement(sevenButton).Click();
            return this;
        }
        public CalculatorPage EightButton()
        {
            driver.FindElement(eightButton).Click();
            return this;
        }
        public CalculatorPage NineButton()
        {
            driver.FindElement(nineButton).Click();
            return this;

        }
        public CalculatorPage ZeroButton()
        {
            driver.FindElement(zeroButton).Click();
            return this;

        }
        public CalculatorPage DotButton()
        {
            driver.FindElement(dotButton).Click();
            return this;

        }
        public CalculatorPage DelButton()
        {
            driver.FindElement(delButton).Click();
            return this;

        }
        public CalculatorPage DivisionButton()
        {
            driver.FindElement(divisionButton).Click();
            return this;

        }
        public CalculatorPage MultiplyButton()
        {
            driver.FindElement(multiButton).Click();
            return this;

        }
        public CalculatorPage MinusButton()
        {
            driver.FindElement(minusButton).Click();
            return this;
        }
        
        

    }
}


