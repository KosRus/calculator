﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;


namespace TestCalculator
{
    [TestFixture]
    public class TestCalc
    {
        
        [Test]
        public void TestCalcs()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_5es2yY");
            caps.AddAdditionalCapability("browserstack.key", "atbgKCuQ6CjPCn6gpbUs");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://7622773d3f3289ac84bf924edd81253f9447694d");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above

            AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                   new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            // Write your custom code here

            


            AndroidElement twobutton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]"));
            AndroidElement buttonPlus = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]"));
            AndroidElement buttonEqual = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]"));
            AndroidElement textField = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView"));

            twobutton.Click();
            buttonPlus.Click();
            twobutton.Click();
            buttonEqual.Click();
            string actual = textField.Text;
            Assert.AreEqual("4", actual);


            // Invoke driver.quit() after the test is done to indicate that the test is completed.
            driver.Quit();

            //[Test]
            //public void Test2()
            //{

            //    AndroidDriver<AndroidElement> driver;

            //    // Write your custom code here

            //    AndroidElement twobutton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]"));

            //    AndroidElement buttonPlus = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]"));


            //    AndroidElement buttonEqual = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]"));
            //    AndroidElement textField = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView"));
            //}
        }
    }
}
