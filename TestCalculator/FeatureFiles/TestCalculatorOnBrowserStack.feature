﻿Feature: TestCalculatorOnBrowserStack
	
	As a user
	I want to add several numbers
	In order to obtaining accurate mathematical calculation
	As a user
	I want to subtracting several numbers
	In order to obtaining accurate mathematical calculation
	As a user
	I want to multiply several numbers
	In order to obtaining accurate mathematical calculation
	As a user
	I want to division several numbers
	In order to obtaining accurate mathematical calculation

Scenario: Add two numbers
	When tap to button two
	When tap to button plus
	When tap to button two
	And tap to button equal
	Then result is four

Scenario: Multiplication of the number four  by seven
	When tap to button four
	When tap to button  multiply
	When tap to button seven
	And tap to button equal
	Then results twenty-seven

Scenario: Dividing the number six by two
	When tap to button six
	When tap to button division
	When tap to button two
	And tap to button equal
	Then results three

Scenario: Dividing the number eight by two and multiply by seven
	When tap to button eight
	When tap to button division
	When tap to button two
	And tap to button equal
	When tap to button  multiply
	When tap to button seven
	Then results twenty-eight

Scenario: Addition of nine  and eight
	When tap to button nine
	When tap to button plus
	When tap to button eight
	And tap to button equal
	Then results seventeen

Scenario: Subtracting the numbers nine and five
	When tap to button nine
	When tap to button minus
	When tap to button five
	And tap to button equal
	Then results four

Scenario:Dividing eight by two and a half
	When tap to button eight
	When tap to button division
	When tap to button two
	When tap to button dot
	When tap to button five
	And tap to button equal
	Then results three and a half

Scenario: Сlear text field before using
	Given  calculator with the number five is open
	When tap to button DEL
	When tap to button one
	When tap to button plus
	When tap to button zero
	And tap to button equal
	Then results one