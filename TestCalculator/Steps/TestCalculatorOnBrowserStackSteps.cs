﻿using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TestCalculator.POM;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace TestCalculator.Steps
{
    [Binding]
    public class CalculatorBrowserStackSteps
    {
        public CalculatorPage calculator;
        public AndroidDriver<AndroidElement> driver;
        [BeforeScenario]
        public void Connect()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_5es2yY");
            caps.AddAdditionalCapability("browserstack.key", "atbgKCuQ6CjPCn6gpbUs");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://7622773d3f3289ac84bf924edd81253f9447694d");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above

            AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                   new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            // Write your custom code here
             calculator = new CalculatorPage(driver);

        }
        [Then(@"result is four")]
        public void ThenResultIsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromResult());
        }


        [When(@"tap to button four")]
        public void WhenTapToButtonFour()
        {
            calculator.FourButton();
        }

        [When(@"tap to button  multiply")]
        public void WhenTapToButtonMultiply()
        {
            calculator.MultiplyButton();
        }

        [When(@"tap to button seven")]
        public void WhenTapToButtonSeven()
        {
            calculator.SevenButton();
        }

        [When(@"tap to button equal")]
        public void WhenTapToButtonEqual()
        {
            calculator.EqualButton();
        }

        [Then(@"results twenty-seven")]
        public void ThenResultsTwenty_Seven()
        {
            Assert.AreEqual("27", calculator.GetTextFromResult());
        }

        [When(@"tap to button six")]
        public void WhenTapToButtonSix()
        {
            calculator.SixButton();
        }

        [When(@"tap to button division")]
        public void WhenTapToButtonDivision()
        {
            calculator.DivisionButton();
        }

        [When(@"tap to button two")]
        public void WhenTapToButtonTwo()
        {
            calculator.TwoButton();
        }

        [Then(@"results three")]
        public void ThenResultsThree()
        {
            Assert.AreEqual("3", calculator.GetTextFromResult());
        }

        [When(@"tap to button eight")]
        public void WhenTapToButtonEight()
        {
            calculator.EightButton();
        }

        [Then(@"results twenty-eight")]
        public void ThenResultsTwenty_Eight()
        {
            Assert.AreEqual("28", calculator.GetTextFromResult());
        }

        [When(@"tap to button nine")]
        public void WhenTapToButtonNine()
        {
            calculator.NineButton();
        }

        [When(@"tap to button plus")]
        public void WhenTapToButtonPlus()
        {
            calculator.AddButton();
        }

        [Then(@"results seventeen")]
        public void ThenResultsSeventeen()
        {
            Assert.AreEqual("17", calculator.GetTextFromResult());
        }

        [When(@"tap to button minus")]
        public void WhenTapToButtonMinus()
        {
            calculator.MinusButton();
        }

        [When(@"tap to button five")]
        public void WhenTapToButtonFive()
        {
            calculator.FiveButton();
        }

        [Then(@"results four")]
        public void ThenResultsFour()
        {
            Assert.AreEqual("4", calculator.GetTextFromResult());
        }

        [When(@"tap to button dot")]
        public void WhenTapToButtonDot()
        {
            calculator.DotButton();
        }

        [Then(@"results three and a half")]
        public void ThenResultsThreeAndAHalf()
        {
            Assert.AreEqual("3,5", calculator.GetTextFromResult());
        }

        [Given(@"calculator with the number five is open")]
        public void GivenCalculatorWithTheNumberFiveIsOpen()
        {
            calculator.FiveButton();
        }

        [When(@"tap to button DEL")]
        public void WhenTapToButtonDEL()
        {
            calculator.DelButton();
        }

        [When(@"tap to button one")]
        public void WhenTapToButtonOne()
        {
            calculator.OneButton();
        }

        [When(@"tap to button zero")]
        public void WhenTapToButtonZero()
        {
            calculator.ZeroButton();
        }

        [Then(@"results one")]
        public void ThenResultsOne()
        {
            Assert.AreEqual("1", calculator.GetTextFromResult());
        }
        [AfterScenario]
        public void Clear()
        {
            calculator.DelButton();
        }
    }
}